<?php
namespace Juspay;
use Juspay\Core\AddCardParams;
use Juspay\Core\Cards;

/**
 * All test for cards are here
*/
class CardsTest extends \PHPUnit_Framework_TestCase
{

    /*
     * Holds an instance of the `Cards` core API class to be used.
     * See constructor.
     */
    private $api;

    /*
     * Holds an instance of the `Cards` API class that is directly instantiated.
     * See the constructor for this class.
     */
    private $apiInstance;

    /*
     * Randomly generated customerId to be used for a test.
     */
    private $customerId;

    /*
     * Params to be used when adding a card.
     */
    private $addCardParams;

    public function __construct()
    {
        parent::__construct();

        /*
         * Configure the Juspay SDK and set up singleton instances of the class.
         * Singletons are set up in the ExpressCheckout class.
         */
        JuspayConfiguration::configureAndSetUp(JuspayConfiguration::ENVIRONMENT_SANDBOX, 'sriduth_sandbox_test', 'F740059B99694ABF83A7C1C879B6892B', 15, 30);

        $this->apiInstance = new Cards(15, 20);
        $this->api = $this->apiInstance;

        $this->customerId = 'test-customer' . rand();
        $this->addCardParams = new AddCardParams($this->customerId, 'sriduth.jayhari@gmail.com', '4242 4242 4242 4242', 12, 20);
    }

    /*
     * The luhn algorithm can be used to check if a given number is a valid card number.
     */
    public function test_luhnCheck()
    {
        $isValid = AddCardParams::luhnCheck("4693750169294547");
        assert($isValid == true);

        $isValid = AddCardParams::luhnCheck("9999999999999999");
        assert(!$isValid);
    }

    /*
     * Add a card to the Juspay locker
     */
    public function test_addCard()
    {
        $response = $this->api->addCard($this->addCardParams);
        print_r($response);
        assert($response['responseCode'] == 200);
    }

    /*
     * Add a card and then delete it.
     */
    public function test_deleteCard()
    {
        $response = $this->api->addCard(new AddCardParams('sriduth_test-2', 'sriduth.jayhari@gmail.com', '4693 7501 6923 4567', 12, 20));
        assert($response['responseCode'] = 200);

        $response = $this->api->deleteCard($response['body']['card_token']);
        assert($response['body']['deleted'] == true);
    }

    /*
     * Add some cards and get a list of cards
     */
    public function test_listCard()
    {
        $cardDetails = array(
            array('4693 7501 6929 4547', 12, 2020),
            array('4565 6734 2341 1234', 12, 2023),
            array('4576 6521 1276 7781', 12, 2023)
        );

        foreach ($cardDetails as $cardDetail)
        {
            $this->api->addCard(new AddCardParams('sriduth-list-2', 'sriduth.jayhari@gmail.com', $cardDetail[0], $cardDetail[1], $cardDetail[2]));
        }

        $response = $this->api->listCards('sriduth-list-2');

        assert($response['responseCode'] == 200);
        assert($response['body']['customer_id'] == 'sriduth-list-2');
        assert($response['body']['merchantId'] == JuspayConfiguration::getMerchantId());

        foreach($response['body']['cards'] as $storedCard)
        {
            $deleteResponse = $this->api->deleteCard($storedCard['card_token']);
            assert($deleteResponse['responseCode'] = 200);
        }
    }

    /*
     * Replace the $api instance by the Singleton created in ExpressCheckout
     */
    public function test_allWithExpressCheckout()
    {
        $this->api = ExpressCheckout::$Card;

        $this->test_addCard();
        $this->test_listCard();
        $this->test_deleteCard();
        $this->test_luhnCheck();

        $this->api = $this->apiInstance;
    }
}
