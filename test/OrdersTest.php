<?php
use Juspay\ExpressCheckout;
use Juspay\JuspayConfiguration;
use Juspay\Core\OrderCreateParams;
use Juspay\Core\OrderListParams;
use Juspay\Core\Orders;
use Juspay\Util\Address;

/**
 * All test for Order related operations that can be performed using the EC api are presented here.
 *
 * @author Sriduth Jayhari
*/
class OrdersTest extends PHPUnit_Framework_TestCase
{
    private $api;
    private $apiInstance;

    private $orderId;

    public function __construct()
    {
        parent::__construct();
        JuspayConfiguration::configureAndSetUp(JuspayConfiguration::ENVIRONMENT_SANDBOX, 'sriduth_sandbox_test', 'F740059B99694ABF83A7C1C879B6892B', 15, 30);
        $this->orderId = 'test-order' . rand();
        $this->apiInstance = new Orders(10, 20);
        $this->api = $this->apiInstance;
    }

    /*
     * Create an order.
     */
    public function test_createOrder()
    {
        $response = $this->api->createOrder(new OrderCreateParams($this->orderId, 'this must be an int'));
        assert($response['body']['status'] === 'ERROR');

        $response = $this->api->createOrder(new OrderCreateParams($this->orderId, 1232));
        assert($response['body']['status'] === 'CREATED');
    }

    /*
     * Create an order, providing the Shipping and billing addresses.
     */
    public function test_orderCreateWithShippingAndBillingAddress()
    {
        $orderId = 'test-order' . rand();

        $address = new Address();

        $address->first_name = 'Sriduth';
        $address->last_name = 'Jayhari';
        $address->city = 'Bangalore';
        $address->country = 'India';
        $address->country_code_iso = 'IND';
        $address->line1 = 'Addr 1';
        $address->line2 = 'Addr 2';
        $address->line3 = 'Addr 3';
        $address->phone = '9632031840';
        $address->postal_code = '560076';

        $orderCreateRequest = new OrderCreateParams($orderId, 100);
        $orderCreateRequest->billing_address = $address;
        $orderCreateRequest->shipping_address = $address;
        $orderCreateRequest->udf = array('asdsad', 'asd', 'asd12wq', 'H671', 'asdasd12', '12AWDWD', '@SAD', 'ASD@$', 'SADSADSAD', 'GSADJK');

        $response = $this->api->createOrder($orderCreateRequest);
        assert($response['body']['status'] == 'CREATED');

        $response = $this->api->getStatus($orderId);
        assert($response['body']['udf1'] === $orderCreateRequest->udf[0]);
        assert($response['body']['udf2'] === $orderCreateRequest->udf[1]);
        assert($response['body']['udf3'] === $orderCreateRequest->udf[2]);
        assert($response['body']['udf4'] === $orderCreateRequest->udf[3]);
        assert($response['body']['udf5'] === $orderCreateRequest->udf[4]);
        assert($response['body']['udf6'] === $orderCreateRequest->udf[5]);
        assert($response['body']['udf7'] === $orderCreateRequest->udf[6]);
        assert($response['body']['udf8'] === $orderCreateRequest->udf[7]);
        assert($response['body']['udf9'] === $orderCreateRequest->udf[8]);
        assert($response['body']['udf10'] === $orderCreateRequest->udf[9]);
    }

    /*
     * Get the status of an order that was created.
     */
    public function test_getOrderStatus()
    {
        // Create the order
        $this->api->createOrder(new OrderCreateParams($this->orderId, 1232));

        // Get the status of the newly created order.
        $response = $this->api->getStatus($this->orderId);
        assert($response['body']['status'] === 'NEW');
    }

    /*
     * List orderss
     */
    public function test_listOrders()
    {
        $response = $this->api->listOrders(new OrderListParams());
        assert($response['responseCode'] === 200);
    }

    /*
     * Update an order.
     *
     * Only the amount of an order can be updated.
     * DO NOT UPDATE THE ORDER AMOUNT AFTER PAYMENT.
     */
    public function test_updateOrder()
    {
        $newOrderId = 'test-order' . rand();
        $response = $this->api->createOrder(new OrderCreateParams($newOrderId, '100'));
        assert($response['body']['status'] === 'CREATED');

        $newAmount = rand();

        $response = $this->api->updateOrder($newOrderId, $newAmount);
        assert($response['body']['amount'] == $newAmount);
    }

    /*
     * Refund an order that was charged.
     *
     */
    public function test_orderRefund()
    {
        $orderId = 'test-order' . rand();
        $response = $this->api->createOrder(new OrderCreateParams($orderId, 100));
        assert($response['body']['status'] == 'CREATED');

        $uniqueId = rand();
        $response = $this->api->refund($uniqueId, $orderId, 100);
        assert($response['body']['status'] === 'invalid_request_error');

        // Make payment for an order ($orderId) and refund the same.
        $orderId = '1467820044';
        $response = $this->api->refund($uniqueId, $orderId, 1);
        assert($response['responseCode'] === 200);
    }

    public function test_allWithExpressCheckout()
    {
        $this->api = ExpressCheckout::$Order;
        
        $this->test_createOrder();
        $this->test_getOrderStatus();
        $this->test_listOrders();
        $this->test_updateOrder();

        $this->api = $this->apiInstance;
    }
}
