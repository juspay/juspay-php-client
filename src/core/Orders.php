<?php
namespace Juspay\Core;

/***
 * All order related APIs are available in this class.
 * For example usage, see unit tests.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 */
class Orders extends JuspayAPI
{
    /***
     * Initialize an instance of the API. Connect timeout and readTimeout MUST be configured.
     *
     * Orders constructor.
     * @param int $connectTimeout
     * @param int $readTimeout
     */
    public function __construct($connectTimeout, $readTimeout)
    {
        parent::__construct($connectTimeout, $readTimeout);
    }

    public function getStatus($order_id)
    {
        return $this->netUtils->doPost('/order/status', array('order_id' => $order_id));
    }

    /**
     * Create an order with the given {@link OrderCreateParams}
     *
     * @param OrderCreateParams $orderCreateParams
     * @return array|mixed
     */
    public function createOrder(OrderCreateParams $orderCreateParams)
    {
        return $this->netUtils->doPost('/order/create', $orderCreateParams->getParams());
    }

    public function listOrders(OrderListParams $orderListParams)
    {
        return $this->netUtils->doPost('/order/list', $orderListParams->getParams());
    }

    public function updateOrder($order_id, $new_amount)
    {
        return $this->netUtils->doPost('/order/update',
            array('order_id' => $order_id, 'amount' => $new_amount));
    }

    public function refund($unique_request_id, $order_id, $amount)
    {
        return $this->netUtils->doPost('/order/refund',
            array('order_id' => $order_id, 'unique_request_id' => $unique_request_id, 'amount' => $amount));
    }
}

/**
 * Wrapper class to encap the fields that cane be specified when creating an order.
 *
 *
 * Class OrderCreateParams
 * @package Juspay\Core
 */
class OrderCreateParams implements Parameterizable
{
    // order_id and amount are mandatory fields to create an order.
    private $order_id;
    private $amount;

    public $udf;

    public $currency;
    public $customer_id;
    public $customer_email;
    public $customer_phone;

    public $description;
    public $product_id;
    public $gateway_id;
    public $return_url;

    public $billing_address;
    public $shipping_address;

    public function __construct($order_id, $amount)
    {
        if(empty($order_id) || empty($amount))
        {
            throw new \InvalidArgumentException('order_id and amount must be specified');
        }

        $this->order_id = $order_id;
        $this->amount = $amount;
    }

    public function getParams()
    {
        $params = array();

        foreach (get_object_vars($this) as $property => $value)
        {
            if(!empty($value) && $property != 'billing_address' && $property != 'shipping_address' && $property != 'udf')
            {
                $params[$property] = $value;
            }
        }

        if(!empty($this->billing_address))
        {
            foreach(get_object_vars($this->billing_address) as $property => $value)
            {
                $params['billing_address' . $property] = $value;
            }
        }

        if(!empty($this->shipping_address))
        {
            foreach(get_object_vars($this->shipping_address) as $property => $value)
            {
                $params['shipping_address' . $property] = $value;
            }
        }

        if(!empty($this->udf))
        {
            for ($i=1; $i<=count($this->udf); $i+=1)
            {
                $params['udf' . ($i)] = $this->udf[$i - 1];
            }
        }
        return $params;
    }
}


class OrderListParams implements Parameterizable
{
    public $created;
    public $gt;
    public $lt;
    public $ge;
    public $le;
    public $count;
    public $offset;
    
    public function __construct($created=null, $gt=null, $lt=null, $ge=null, $le=null, $count=null, $offset=null)
    {
        $this->created = $created;
        $this->gt = $gt;
        $this->lt = $lt;
        $this->ge = $ge;
        $this->le = $le;
        $this->count = $count;
        $this->offset = $offset;
    }


    public function getParams()
    {
        $params = array();
        foreach (get_object_vars($this) as $var => $value)
        {
            if(!empty($value))
            {
                $params[$var] = $value;
            }
        }

        return $params;
    }
}
