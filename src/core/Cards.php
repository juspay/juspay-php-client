<?php
namespace Juspay\Core;

/**
 * Implements the card API.
 *
 * For usage, see the unit test classes.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 */
class Cards extends JuspayAPI
{
    public function __construct($connectTimeout, $readTimeout)
    {
        parent::__construct();
    }

    /***
     * Save a card and add it to the Juspay card locker.
     * Note that a locker needs to be enabled for a merchant account for a card to be saved.
     *
     * @param AddCardParams $params
     * @return array|mixed
     * @throws \InvalidArgumentException
     */
    public function addCard(AddCardParams $params)
    {
        return $this->netUtils->doPost('/card/add', $params->getParams());
    }

    /***
     * Lists all saved cards for a given customer.
     *
     * @param $customer_id
     * @return array|mixed
     */
    public function listCards($customer_id)
    {
        if(empty($customer_id))
        {
            throw new \InvalidArgumentException('customer_id can not be null');
        }

        return $this->netUtils->doPost('/card/list', array('customer_id' => $customer_id));
    }

    /***
     * Delete a saved card given the card token.
     *
     * @param $card_token
     * @return array|mixed
     */
    public function deleteCard($card_token)
    {
        if(empty($card_token))
        {
            throw new \InvalidArgumentException('card_token can not be null');
        }


        return $this->netUtils->doPost('/card/delete', array('card_token' => $card_token));
    }
}

/***
 * Wrapper class to initialize parameters required to add a card;
 * Basic validation methods are present to validate the card, although the validations are not present.
 *
 * Class AddCardParams
 * @package Juspay\Core
 */
class AddCardParams implements Parameterizable
{
    private $customer_id;
    private $customer_email;
    private $card_number;
    private $exp_year;
    private $exp_month;
    public $name_on_card;
    public $nickname;

    public static function validateCardExpiryDate($month, $year)
    {
        $thisYear = date('Y');
        if($year > $thisYear)
        {
            return true;
        }
        else if($year == $thisYear)
        {
            return $month > date('m');
        }

        return false;
    }

    /**
     * Implementation of the luhn algorithm to check if a given card number is a valid.
     *
     * @param $cardNumber
     * @return bool
     */
    public static function luhnCheck($cardNumber)
    {
        $cardNumber = array_reverse(str_split($cardNumber));
        $alternate = false;

        $value = array_reduce($cardNumber, function ($total, $n) use (&$alternate) {
            $n = (int)$n;

            if($alternate)
            {
                $n *= 2;
                if($n > 9)
                {
                    $n = ($n % 10) + 1;
                }
            }

            $total += $n;
            $alternate = !$alternate;

            return $total;
        });

        return $value % 10 == 0;
    }

    /**
     * AddCardParams constructor. The constructor has all Compulsory fields required to add a card.
     * @param $customer_id
     * @param $customer_email
     * @param $card_number
     * @param $exp_year
     * @param $exp_month
     */
    public function __construct($customer_id, $customer_email, $card_number, $exp_month, $exp_year)
    {
        $this->customer_id = $customer_id;
        $this->customer_email = $customer_email;
        $this->card_number = $card_number;
        $this->exp_month = $exp_month;
        $this->exp_year = $exp_year;
    }

    public function getParams()
    {
        $params = array();
        foreach (get_object_vars($this) as $property => $value)
        {
            if(empty($value) && ($property != 'name_on_card' && $property != 'nickname'))
            {
                throw new \InvalidArgumentException(
                    '$customer_id,$customer_email,$card_number,$exp_year,$exp_month must be defined'
                );
            }
            else
            {
                $params[$property] = $value;
            }
        }

        return $params;
    }

    /**
     * @return mixed
     */
    public function getNameOnCard()
    {
        return $this->name_on_card;
    }

    /**
     * @param mixed $name_on_card
     */
    public function setNameOnCard($name_on_card)
    {
        $this->name_on_card = $name_on_card;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param mixed $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }
}