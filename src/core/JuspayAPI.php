<?php
namespace Juspay\Core;

use Juspay\JuspayConfiguration;
use Juspay\Util\NetUtils;

abstract class JuspayAPI
{
    protected $netUtils;

    public function __construct($connectTimeout=15, $readTimeout=30)
    {
        if(!JuspayConfiguration::isConfigured())
        {
            throw new \RuntimeException('Environment is not configured correctly');
        }
        $this->netUtils = new NetUtils($connectTimeout, $readTimeout);
    }

}

interface Parameterizable
{
    public function getParams();
}