<?php
namespace Juspay\Util;

/**
 * Simple address definition that can be used to add shipping and billing addresses.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 */
class Address
{
    public $first_name;
    public $last_name;

    public $line1;
    public $line2;
    public $line3;

    public $city;
    public $state;
    public $country;

    public $postal_code;
    public $phone;
    public $country_code_iso;
}
