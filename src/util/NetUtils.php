<?php
namespace Juspay\Util;

use Juspay\JuspayConfiguration;

/**
 * Wrapper around libcurl.
 *
 * If Content-Type of response is application/json, json_decode is
 * used to parse the response into associative arrays.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 * @package Juspay\Util
 */
class NetUtils
{
    private $_connectTimeout;
    private $_readTimeout;

    function __construct($_connect_timeout=15, $_read_timeout=30)
    {
        $this->_connectTimeout = $_connect_timeout;
        $this->_readTimeout = $_read_timeout;
    }

    /**
     * Sends a POST request to a URL, given the URI and parameters.
     * By default we send the API key via HTTP Auth.
     *
     * If content type of response is application/json we parse the response body
     * to an associative array.
    */
    public function doPost($uri, $post_arguments) {
        $api_url = JuspayConfiguration::getBaseUrl() . $uri;

        $curl_object = curl_init($api_url);
        curl_setopt($curl_object, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_object, CURLOPT_HEADER, true);
        curl_setopt($curl_object, CURLOPT_NOBODY, false);
        curl_setopt($curl_object, CURLOPT_USERPWD, JuspayConfiguration::getApiKey());
        curl_setopt($curl_object, CURLOPT_POST, 1);

        curl_setopt($curl_object, CURLOPT_POSTFIELDS, $post_arguments);
        curl_setopt($curl_object, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl_object, CURLOPT_TIMEOUT, $this->_readTimeout);
        curl_setopt($curl_object, CURLOPT_CONNECTTIMEOUT, $this->_connectTimeout);

        $response = curl_exec($curl_object);

        $header_size = curl_getinfo($curl_object, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        if(explode(';', curl_getinfo($curl_object, CURLINFO_CONTENT_TYPE))[0] === 'application/json')
        {
            $body = json_decode($body, true);
        }

        $response = array(
            'body' => $body,
            'headers' => $headers,
            'responseCode' => curl_getinfo($curl_object, CURLINFO_HTTP_CODE)
        );

        if(!$response)
        {
            $response['error'] = curl_errno($curl_object);
        }

        curl_close($curl_object);

        return $response;
    }
}