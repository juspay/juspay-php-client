<?php
namespace Juspay;
use Juspay\Core\Cards;
use Juspay\Core\Orders;

/**
 * Holder class for singleton API objects.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 */
class ExpressCheckout
{
    public static $Card;
    public static $Order;

    public static function setUp($connectTimeout, $readTimeout)
    {
        self::$Card = new Cards($connectTimeout, $readTimeout);
        self::$Order = new Orders($connectTimeout, $readTimeout);
    }
}