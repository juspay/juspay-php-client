<?php
namespace Juspay;

/**
 * Configuration holder for the ExpressCheckout SDK.
 *
 * @author Sriduth Jayhari <sriduth.jayhari@juspay.in>
 */
class JuspayConfiguration {

    const ENVIRONMENT_PRODUCTION = 'https://api.juspay.in';
    const ENVIRONMENT_SANDBOX = 'https://sandbox.juspay.in';
    const ENVIRONMENT_LOCAL = 'http://localhost:8080';

    private static $MERCHANT_ID;
    private static $API_KEY;
    private static $ENVIRONMENT;

    private static $configured = false;

    public static function configureAndSetUp($environment, $merchantId, $apiKey, $apiConnectTimeout, $apiReadTimeout)
    {
        self::configure($environment, $merchantId, $apiKey);
        ExpressCheckout::setUp($apiConnectTimeout, $apiReadTimeout);
    }

    public static function configure($environment, $merchantId, $apiKey)
    {
        if($environment != self::ENVIRONMENT_LOCAL && $environment != self::ENVIRONMENT_SANDBOX && $environment != self::ENVIRONMENT_PRODUCTION)
        {
            throw new \InvalidArgumentException('Environment is not configured correctly');
        }

        if(empty($merchantId) || empty($apiKey))
        {
            throw new \InvalidArgumentException('merchantId and apiKey must be configured');
        }

        self::$MERCHANT_ID = $merchantId;
        self::$API_KEY = $apiKey;
        self::$ENVIRONMENT = $environment;

        self::$configured = true;
    }

    /**
     * @return mixed
     */
    public static function getMerchantId()
    {
        return self::$MERCHANT_ID;
    }

    /**
     * @return mixed
     */
    public static function getApiKey()
    {
        return self::$API_KEY;
    }

    /**
     * @return mixed
     */
    public static function getEnvironment()
    {
        return self::$ENVIRONMENT;
    }

    public static function getBaseUrl()
    {
        return self::$ENVIRONMENT;
    }

    /**
     * @return boolean
     */
    public static function isConfigured()
    {
        return self::$configured;
    }
}
